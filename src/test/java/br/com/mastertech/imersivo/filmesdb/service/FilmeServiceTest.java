package br.com.mastertech.imersivo.filmesdb.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.mastertech.imersivo.filmesdb.model.Filme;
import br.com.mastertech.imersivo.filmesdb.model.FilmeDTO;
import br.com.mastertech.imersivo.filmesdb.repository.FilmeRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { FilmeService.class })
public class FilmeServiceTest {
	@Autowired
	FilmeService filmeService;

	@MockBean
	FilmeRepository filmeRepository;

	@Test
	public void deveListarTodosOsFilmes() {

		List<Filme> filmes = new ArrayList<>();
		filmes.add(criarFilme(1L));

		when(filmeRepository.findAll()).thenReturn(filmes);

		Iterable<Filme> resultado = filmeService.obterFilmes();

		assertEquals(1, Lists.list(resultado).size());
	}

	@Test
	public void deveListarTodosOsFilmesDeUmDiretor() {
		long id = 1;

		FilmeDTO filmeDTO = mock(FilmeDTO.class);
		List<FilmeDTO> filmes = new ArrayList<>();
		filmes.add(filmeDTO);

		when(filmeRepository.findAllByDiretor_Id(id)).thenReturn(filmes);

		List<FilmeDTO> resultado = Lists.newArrayList(filmeService.obterFilmes(id));

		assertEquals(filmeDTO, resultado.get(0));
	}

	@Test
	public void deveCriarUmFilme() {
		Filme filme = criarFilme(1L);

		filmeService.criarFilme(filme);

		Mockito.verify(filmeRepository).save(filme);
	}

	@Test
	public void deveEditarUmFilme() {
		long id = 1;
		String novoGenero = "Comédia";
		Filme filmeDoBanco = criarFilme(id);
		Filme filmeDoFront = criarFilme(id);
		filmeDoFront.setGenero(novoGenero);
		
		when(filmeRepository.findById(id)).thenReturn(Optional.of(filmeDoBanco));
		when(filmeRepository.save(filmeDoBanco)).thenReturn(filmeDoBanco);
		
		Filme resultado = filmeService.editarFilme(id, filmeDoFront);

		assertNotNull(resultado);
		assertEquals(novoGenero, resultado.getGenero());
	}
	
	@Test(expected = ResponseStatusException.class)
	public void deveLancarExecaoQuandoNaoEncontrarOFilme() {
		long id = 1;
		long novoId = 10;
		Filme filmeDoFront = criarFilme(novoId);
		
		when(filmeRepository.findById(id)).thenReturn(Optional.empty());
		
		filmeService.editarFilme(id, filmeDoFront);
	}

	@Test
	public void deveApagarUmFilme() {
		Long id = 1L;
		Filme filmeDoFront = criarFilme(id);
		
		when(filmeRepository.findById(id)).thenReturn(Optional.of(filmeDoFront));
		
		filmeService.apagarFilme(id);
		
		Mockito.verify(filmeRepository).delete(filmeDoFront);
	}
	
	private Filme criarFilme(Long id) {
		Filme filme = new Filme();
		filme.setId(id);
		filme.setGenero("Ação");
		filme.setTitulo("Matrix");

		return filme;
	}
}
