package br.com.mastertech.imersivo.filmesdb.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.mastertech.imersivo.filmesdb.model.Diretor;
import br.com.mastertech.imersivo.filmesdb.model.DiretorDTO;
import br.com.mastertech.imersivo.filmesdb.model.FilmeDTO;
import br.com.mastertech.imersivo.filmesdb.repository.DiretorRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {DiretorService.class})
public class DiretorServiceTest {

	@Autowired
	DiretorService diretorService;

	@MockBean
	DiretorRepository diretorRepository;
	
	@MockBean
	FilmeService filmeService;

	@Test
	public void deveListarTodosOsDiretores() {

		List<Diretor> diretores = new ArrayList<>();
		diretores.add(new Diretor());

		Mockito.when(diretorRepository.findAll()).thenReturn(diretores);

		Iterable<Diretor> resultado = diretorService.obterDiretores();

		assertEquals(1, Lists.list(resultado).size());
	}
	
	@Test
	public void deveCriarUmDiretor() {

		Diretor diretor = new Diretor();

		diretorService.criarDiretor(diretor);

		Mockito.verify(diretorRepository).save(diretor);
	}
	
	@Test
	public void deveApagarUmDiretor() {
		Long id = 1L;
		Diretor diretorDoFront = new Diretor();
		diretorDoFront.setId(id);
		
		when(diretorRepository.findById(id)).thenReturn(Optional.of(diretorDoFront));
		
		diretorService.apagarDiretor(id);
		
		Mockito.verify(diretorRepository).delete(diretorDoFront);
	}
	
	@Test(expected = ResponseStatusException.class)
	public void deveLancarExecaoQuandoNaoEncontrarODiretor() {
		long id = 1;
		
		when(diretorRepository.findById(id)).thenReturn(Optional.empty());
		
		diretorService.apagarDiretor(id);
	}
	
	@Test
	public void deveListarTodosOsFilmesDeUmDiretor() {
		long id = 1;

		FilmeDTO filmeDTO = mock(FilmeDTO.class);
		List<FilmeDTO> filmes = new ArrayList<>();
		filmes.add(filmeDTO);

		when(filmeService.obterFilmes(id)).thenReturn(filmes);

		List<FilmeDTO> resultado = Lists.newArrayList(diretorService.obterFilmes(id));

		assertEquals(filmeDTO, resultado.get(0));
	}
	
	@Test
	public void deveListarUmDiretorDTO() {
		long id = 1;

		DiretorDTO diretorDTO = mock(DiretorDTO.class);

		when(diretorRepository.findSummary(id)).thenReturn(diretorDTO);

		DiretorDTO resultado = diretorService.obterDiretor(id);

		assertEquals(diretorDTO, resultado);
	}
}
